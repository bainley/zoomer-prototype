# zoomer-prototype

Quick little vue3 prototype to show 2 different approaches of creating an animated photo gallery with a kind of "camera fly though" effect.

One example uses `window.requestAnimationFrame` to animate the depth axis over appended time.

Another example uses an animation library ([gsap](https://greensock.com/)) to animate the depth over a tween that gets repeated over and over again.

## Project setup
```
yarn install
```

### Compiles and hot-reloads for development
```
yarn serve
```

### Compiles and minifies for production
```
yarn build
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
