import { createRouter, createWebHashHistory } from 'vue-router'
import ZoomerRafView from '../views/ZoomerRafView.vue'
import ZoomerGsapView from '../views/ZoomerGsapView.vue'

const routes = [
  {
    path: '/raf',
    name: 'raf',
    component: ZoomerRafView,
  },
  {
    path: '/gsap',
    name: 'gsap',
    component: ZoomerGsapView,
  },
  {
    path: '/:catchAll(.*)',
    redirect: '/raf',
  }
]

const router = createRouter({
  history: createWebHashHistory(),
  routes
})

export default router
